﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private System.Timers.Timer timer;
        public TimerService()
        {
            
            timer = new System.Timers.Timer();
            timer.AutoReset = true;
            timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, e);
        }

        public double Interval { get; set; }



        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            timer.Elapsed -= Timer_Elapsed;
            timer.Dispose();
        }

        public void Start()
        {
            timer.Interval = Interval;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}