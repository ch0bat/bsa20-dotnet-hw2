﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;


namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logPath)
        {
            this.LogPath = logPath;
        }
        public string LogPath { get; }

        public string Read()
        {
            return File.ReadAllText(LogPath);
        }

        public void Write(string logInfo)
        {
            File.AppendAllText(LogPath, logInfo+Environment.NewLine);
        }
    }
}
