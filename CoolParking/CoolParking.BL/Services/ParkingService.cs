﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private List<TransactionInfo> transactions = new List<TransactionInfo>();
        private readonly ILogService logService;
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private static readonly Parking parking = Parking.Instance;
        private static string filePath = $@"{ Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        public static IParkingService Instance = new ParkingService(new TimerService(), new TimerService(), new LogService(filePath));

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            
            this.logService = logService;
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            //parking = new Parking(Settings.ParkingCapacity, Settings.ParkingBalance);
            //parking = Parking.Instance;
            parking.Vehicles = new List<Vehicle>();
            this.withdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            this.logTimer.Elapsed += LogTimer_Elapsed;
            this.withdrawTimer.Interval = Settings.FeeChargeInterval.TotalMilliseconds;
            this.logTimer.Interval = Settings.LogWriteInterval.TotalMilliseconds;
            this.withdrawTimer.Start();
            this.logTimer.Start();
        }

        private void LogTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
          

           
            var sb = new StringBuilder();
            foreach (var item in transactions)
            {
                sb.AppendLine(item.ToString());
            }
            logService.Write(sb.ToString());
            transactions.Clear();
        }

        private void WithdrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var vehicles = this.GetVehicles();
            foreach (var vehicle in vehicles)
            {
                var fee = Settings.GetVehicleRates((int)vehicle.Type);
                decimal charge;
                if (vehicle.Balance < 0)
                {
                    charge = fee * Settings.FineCoef;
                }
                else
                {
                    charge = vehicle.Balance - fee;
                    if (charge < 0)
                    {
                        charge = -charge * Settings.FineCoef + vehicle.Balance;
                    }
                    else
                    {
                        charge = fee;
                    }
                }
                vehicle.Balance -= charge;
                parking.Balance += charge;
                transactions.Add(new TransactionInfo(vehicle.Id, charge));
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new OverflowException("You vialated the limit");
            if (parking.Vehicles.Select(v => v.Id).Contains(vehicle.Id))
                throw new ArgumentException("Vehicle with this Id is already exists in the parking");
            parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            withdrawTimer.Stop();
            logTimer.Stop();
            withdrawTimer.Elapsed -= WithdrawTimer_Elapsed;
            logTimer.Elapsed -= LogTimer_Elapsed;
            withdrawTimer.Dispose();
            logTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return parking.Capacity - parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.Vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = parking.Vehicles.SingleOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException("There is no such vehicle in the parking");
            parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = parking.Vehicles.SingleOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
            {
                Console.WriteLine("There is no such vehicle in the parking");
                //throw new ArgumentException("There is no such vehicle in the parking");
                //if (sum < 0)
                //    throw new ArgumentException("Sum can not be less than 0");
                //vehicle.Balance += sum;
            }
            else
            {
                if (sum < 0)
                    throw new ArgumentException("Sum can not be less than 0");
                vehicle.Balance += sum;
            }
        }
    }
}
