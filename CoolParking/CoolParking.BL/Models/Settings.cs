﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal ParkingBalance = 0;
        public static int ParkingCapacity = 10;

        public static TimeSpan FeeChargeInterval = TimeSpan.FromSeconds(5);
        public static TimeSpan LogWriteInterval = TimeSpan.FromSeconds(60);
        public static decimal GetVehicleRates(int type)
        => (VehicleType)type switch
        {
            VehicleType.Bus => 3.5m,
            VehicleType.PassengerCar => 2,
            VehicleType.Motorcycle => 1,
            VehicleType.Truck => 5,
            _ => throw new ArgumentException("Invalid Enum Value" + type)
        };
        public static decimal FineCoef = 2.5m;

    }
}