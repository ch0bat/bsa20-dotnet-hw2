﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        //singleton
        private static Parking instance;
        public List<Vehicle> Vehicles; 
        public Parking(int capacity, decimal balance)
        {
            Capacity = capacity;
            Balance = balance;
        }
        public decimal Balance { get; set; }
        public int Capacity { get; }

       // singleton
        public static Parking Instance
        {
            get
            {
                if (instance == null)
                    instance = new Parking(Settings.ParkingCapacity, Settings.ParkingBalance);
                return instance;
            }
        }

    }
}