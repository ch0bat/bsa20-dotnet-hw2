﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rand = new Random();
            string Y1 = "";
            string Y2 = "";
            char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            for (int j = 1; j <= 2; j++)
            {
                int letter_num = rand.Next(0, letters.Length - 1);
                Y1 += letters[letter_num];
            }
            for (int j = 1; j <= 2; j++)
            {
                int letter_num = rand.Next(0, letters.Length - 1);
                Y2 += letters[letter_num];
            }
            return $"{Y2}-{rand.Next(0, 9)}{rand.Next(0, 9)}{rand.Next(0, 9)}{rand.Next(0, 9)}-{Y1}";
        }
        public static Regex Regex = new Regex(@"([A-Z]{2}-[0-9]{4}-[A-Z]{2})");
        public Vehicle()
        {

        }
        public Vehicle(string id, VehicleType type, decimal balance)
        {
            var isMatch = Regex.IsMatch(id);
            if (!isMatch)
                throw new System.ArgumentException("Invalid Id format");
            if (balance <= 0)
                throw new System.ArgumentException("Invalid balance");
            Id = id;
            Type = type;
            Balance = balance;
        }
        public VehicleType Type { get; set; }

        public string Id { get; set; }

        public decimal Balance { get; set; }
    }
}
