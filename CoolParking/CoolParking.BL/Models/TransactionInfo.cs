﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(string id, decimal charge)
        {
            Id = id;
            Sum = charge;
            Created = DateTime.Now;
        }

        public string Id { get; }
        public decimal Sum { get; }

        public DateTime Created { get; }

        public override string ToString()
        {
            return $"{Created}: {Id} {Sum}";
        }
    }
}