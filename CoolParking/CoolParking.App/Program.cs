﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.App
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            await DoWork();
        }
        
        private static async Task DoWork()
        {
            var filePath = $@"{ Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            var parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(filePath));
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44335/api/");
           
            while (true)
            {
                Console.WriteLine("1. Show parking balance");
                Console.WriteLine("2. Show parkin capacity");
                Console.WriteLine("3. Show free places");
                Console.WriteLine("4. Show all transactions ");
                Console.WriteLine("5. Show last transaction");
                Console.WriteLine("6. Show vehicles list in the parking");
                Console.WriteLine("7. Add vehicle");
                Console.WriteLine("8. Remove vehicle");
                Console.WriteLine("9. Top Up Vehicle");
                Console.WriteLine("10. Show vehicles by Id");
                Console.WriteLine("0. Close aplication");

                Console.Write("Choose number:");
                var number = Console.ReadLine();
                switch (number)
                {
                    case "1":
                        {
                            var response = await client.GetAsync("parking/balance/");
                            var balance = JsonConvert.DeserializeObject<decimal>(await response.Content.ReadAsStringAsync());

                            /*var balance = parkingService.GetBalance();*/
                            Console.WriteLine($"Current parking balance: {balance}");
                            Console.WriteLine();
                        }
                        break;
                    case "2":
                        {
                            var response = await client.GetAsync("parking/capacity/");
                            var capacity = JsonConvert.DeserializeObject<int>(await response.Content.ReadAsStringAsync());

                            /*var balance = parkingService.GetBalance();*/
                            Console.WriteLine($"Current parking balance: {capacity}");
                            Console.WriteLine();
                        }
                        break;
                    case "3":
                        {
                            var response = await client.GetAsync("parking/freePlaces/");
                            var free = JsonConvert.DeserializeObject<int>(await response.Content.ReadAsStringAsync());

                           
                            //var free = parkingService.GetFreePlaces();
                            Console.WriteLine($"Free parking places: {free}");
                            Console.WriteLine();
                        }
                        break;
                    case "4":
                        {
                            var response = await client.GetAsync("transactions/all");
                            var json = await response.Content.ReadAsStringAsync();
                            var transactions = JArray.Parse(json); // JsonConvert.DeserializeObject<IEnumerable<TransactionInfo>>(json);

                            if (transactions.Count() != 0)
                            {
                               
                                Console.WriteLine("----");
                                foreach (var item in transactions)
                                {
                                    Console.WriteLine($"{item["created"]}: {item["id"]}, {item["sum"]}");
                                }
                            }
                            else
                            {
                                Console.WriteLine("There are no transactions avalible");
                            }

                            Console.WriteLine();
                        }
                        break;
                    case "5":
                        {

                            var response = await client.GetAsync("transactions/last");
                            //var last = await response.Content.ReadAsStringAsync();
                            // JsonConvert.DeserializeObject<IEnumerable<TransactionInfo>>(json);
                            //var free = JsonConvert.DeserializeObject<TransactionInfo>(await response.Content.ReadAsStringAsync());

                            try
                            {
                                var last = await response.Content.ReadAsStringAsync();
                                Console.WriteLine(last);

                            }
                            catch(Exception ex)
                            {
                                Console.WriteLine("There are no transactions avalible");
                            }

                            Console.WriteLine();
                        }
                        break;
                    case "6":
                        {
                            var response = await client.GetAsync("vehicles");
                            var list = JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(await response.Content.ReadAsStringAsync());
                            //var list = parkingService.GetVehicles();
                            foreach (var vehicle in list)
                            {
                                Console.WriteLine($"{vehicle.Id} {(int)vehicle.Type} {vehicle.Balance}");
                            }
                            Console.WriteLine();
                        }
                        break;
                    case "7":
                        {
                            Console.Write("Enter vehicle to add (VehicleType Balance): ");
                            var vehicleStr = Console.ReadLine();
                            Vehicle vehicle = null;
                            try
                            {
                                var vehicleArgs = vehicleStr.Split();
                                var id = Vehicle.GenerateRandomRegistrationPlateNumber();
                                var type = (VehicleType)Enum.Parse(typeof(VehicleType), vehicleArgs[0]);
                                var balance = decimal.Parse(vehicleArgs[1]);
                               
                                //var response = await client.PostAsJsonAsync("vehicles", vehicle);

                                try
                                {
                                    
                                   vehicle = new Vehicle(id, type, balance);
                                   
                                        var response = await client.PostAsJsonAsync("vehicles", vehicle);
                                  
                                }
                                catch (ArgumentException ex)
                                {
                                    

                                    Console.WriteLine(ex.Message);
                                }
                            }
                            catch
                            {
                                var response = await client.PostAsJsonAsync("vehicles", vehicle);
                                Console.WriteLine("Invalid Vehicle Format");
                            }
                        }
                        break;
                    case "8":
                        {
                            Console.Write("Enter Vehicle Id to remove: ");
                            var vehicleId = Console.ReadLine();
                            try
                            {
                                var isMatch = Vehicle.Regex.IsMatch(vehicleId);
                                var response = await client.DeleteAsync($"vehicles/{vehicleId}");
                                if (!isMatch)
                                    throw new System.ArgumentException("Invalid Id format");

                                //response = await client.DeleteAsync($"vehicles/{vehicleId}");
                            }
                            catch (ArgumentException ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            
                        }
                        break;
                    case "9":
                        {
                            Console.Write("Enter Vehicle Id and sum to TopUp (Id Sum): ");
                            var vehicleStr = Console.ReadLine();
                            string[] vehicleArgs = null;
                            string id = null;
                            decimal sum = 0;
                            try
                            {
                               vehicleArgs = vehicleStr.Split();
                               id = vehicleArgs[0];
                                sum = decimal.Parse(vehicleArgs[1]);
                                try
                                {
                                    var response = await client.PutAsJsonAsync("transactions/topUpVehicle/", new { VehicleId = id, Sum = sum });
                                var isMatch = Vehicle.Regex.IsMatch(id);
                                    if (!isMatch)
                                    {
                                        //var response = await client.PutAsJsonAsync("transactions/topUpVehicle/", new { VehicleId = id, Sum = sum });
                                        throw new System.ArgumentException("Invalid Id format");
                                    }

                                   //var response2 = await client.PutAsJsonAsync("transactions/topUpVehicle/", new { VehicleId = id, Sum = sum });
                                    var vehicle = JsonConvert.DeserializeObject<Vehicle>(await response.Content.ReadAsStringAsync());
                                    if (vehicle != null)
                                    {
                                        Console.WriteLine($"{vehicle.Id} {vehicle.Type} {vehicle.Balance}");
                                    }
                                    else
                                        Console.WriteLine("There is no such vehicle in the parking");
                                }
                                catch (ArgumentException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            catch
                            {
                                var response = await client.PutAsJsonAsync("transactions/topUpVehicle/", new { VehicleId = id, Sum = sum });
                                Console.WriteLine("Invalid TopUp Format");
                            }
                        }
                        break;
                    case "10":
                        Console.Write("Enter Vehicle Id: ");
                        var vehicleById = Console.ReadLine();
                        try
                        {
                            var isMatch = Vehicle.Regex.IsMatch(vehicleById);
                            var respons = await client.GetAsync($"vehicles/{vehicleById}");
                            if (!isMatch)
                            {
                                //var respons = await client.GetAsync($"vehicles/{vehicleById}");
                                var last2 = await respons.Content.ReadAsStringAsync();
                                Console.WriteLine(last2);
                                throw new System.ArgumentException("Invalid Id format");
                            }
                            
                          var response = await client.GetAsync($"vehicles/{vehicleById}");
                            var last = await response.Content.ReadAsStringAsync();
                            Console.WriteLine(last);
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case "0":
                        return;
                    default:
                        Console.WriteLine("Invalid Operation " + number);
                        break;

                }
            }

        }


    }
}
