using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CoolParking.WebAPI
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            await DoWork();
        }

        private static async Task DoWork()
        {
            var filePath = $@"{ Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            var parkingService = new ParkingService(new TimerService(), new TimerService(), new LogService(filePath));
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44335/api/");
            while (true)
            { 

                Console.WriteLine("1. Show parking balance");
                Console.WriteLine("2. Show sum of transactions of current period");
                Console.WriteLine("3. Show free places");
                Console.WriteLine("4. Show all transactions for current period");
                Console.WriteLine("5. Show history transactions from log");
                Console.WriteLine("6. Show vehicles list in the parking");
                Console.WriteLine("7. Add vehicle");
                Console.WriteLine("8. Remove vehicle");
                Console.WriteLine("9. Top Up Vehicle");
                Console.WriteLine("0. Close aplication");

                Console.Write("Choose number:");
                var number = Console.ReadLine();
                switch (number)
                {
                    case "1":
                        {
                            var response = await client.GetAsync("parking/balance/");
                            var balance = JsonConvert.DeserializeObject<decimal>(await response.Content.ReadAsStringAsync());

                            /*var balance = parkingService.GetBalance();*/
                            Console.WriteLine($"Current parking balance: {balance}");
                            Console.WriteLine();
                        }
                        break;
                    case "2":
                        {
                            var sum = parkingService.GetLastParkingTransactions().Sum(t => t.Sum);
                            Console.WriteLine($"Current period sum: {sum}");
                        }
                        break;
                    case "3":
                        {
                            var response = await client.GetAsync("parking/freePlaces/");
                            var free = JsonConvert.DeserializeObject<int>(await response.Content.ReadAsStringAsync());


                            //var free = parkingService.GetFreePlaces();
                            Console.WriteLine($"Free parking places: {free}");
                            Console.WriteLine();
                        }
                        break;
                    case "4":
                        {
                            var transactions = parkingService.GetLastParkingTransactions();
                            if (transactions.Length != 0)
                            {
                                foreach (var item in parkingService.GetLastParkingTransactions())
                                {
                                    Console.WriteLine(item.ToString());
                                }
                            }
                            else
                            {
                                Console.WriteLine("There are no transactions avalible");
                            }

                            Console.WriteLine();
                        }
                        break;
                    case "5":
                        {
                            Console.WriteLine(parkingService.ReadFromLog());
                        }
                        break;
                    case "6":
                        {
                            var response = await client.GetAsync("vehicles/");
                            var list = JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(await response.Content.ReadAsStringAsync());
                            //var list = parkingService.GetVehicles();
                            foreach (var vehicle in list)
                            {
                                Console.WriteLine($"{vehicle.Id} {vehicle.Type} {vehicle.Balance}");
                            }
                            Console.WriteLine();
                        }
                        break;
                    case "7":
                        {
                            Console.Write("Enter vehicle to add (VehicleType Balance): ");
                            var vehicleStr = Console.ReadLine();
                            try
                            {
                                var vehicleArgs = vehicleStr.Split();
                                var id = Vehicle.GenerateRandomRegistrationPlateNumber();
                                var type = (VehicleType)Enum.Parse(typeof(VehicleType), vehicleArgs[0]);
                                var balance = decimal.Parse(vehicleArgs[1]);
                                try
                                {
                                    var vehicle = new Vehicle(id, type, balance);

                                    var response = await client.PostAsync("vehicles/",
                                        new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.Default, "application/json"));
                                }
                                catch (ArgumentException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            catch
                            {
                                Console.WriteLine("Invalid Vehicle Format");
                            }
                        }
                        break;
                    case "8":
                        {
                            Console.Write("Enter Vehicle Id to remove: ");
                            var vehicleId = Console.ReadLine();
                            try
                            {
                                var isMatch = Vehicle.Regex.IsMatch(vehicleId);
                                if (!isMatch)
                                    throw new System.ArgumentException("Invalid Id format");
                                parkingService.RemoveVehicle(vehicleId);
                            }
                            catch (ArgumentException ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                        break;
                    case "9":
                        {
                            Console.Write("Enter Vehicle Id and sum to TopUp (Id Sum): ");
                            var vehicleStr = Console.ReadLine();
                            try
                            {
                                var vehicleArgs = vehicleStr.Split();
                                var id = vehicleArgs[0];
                                var sum = decimal.Parse(vehicleArgs[1]);
                                try
                                {
                                    var isMatch = Vehicle.Regex.IsMatch(id);
                                    if (!isMatch)
                                        throw new System.ArgumentException("Invalid Id format");
                                    parkingService.TopUpVehicle(id, sum);
                                }
                                catch (ArgumentException ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            catch
                            {
                                Console.WriteLine("Invalid TopUp Format");
                            }
                        }
                        break;
                    case "0":
                        return;
                    default:
                        Console.WriteLine("Invalid Operation " + number);
                        break;

                }
            }

        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
