﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class VehiclesController : Controller
    {
        IParkingService parkingService;

        public VehiclesController()
        {
            parkingService = ParkingService.Instance;
        }
        // GET: api/<controller>
        //        Response:
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: [{ “id”: string, “vehicleType”: int, "balance": decimal
        //    }]
        //Body example: [{ “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }]
        [HttpGet]
        public IActionResult Get()
        {
                        return Ok(parkingService.GetVehicles());
        }

        // GET: api/<controller>/{id}
        //        Response:
        //If id is invalid - Status Code: 400 Bad Request
        //If vehicle not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: { “id”: string, “vehicleType”: int, "balance": decimal
        //    }
        //    Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            var isMatch = Vehicle.Regex.IsMatch(id);
            if (isMatch)
            {
                var vehicle = parkingService.GetVehicles().SingleOrDefault(v => v.Id == id);
                                if (vehicle != null)
                {
                    return Ok(vehicle);
                }
                //if (!isMatch)
                //{
                //    return BadRequest(vehicle);
                //}
                else
                {
                    return NotFound(vehicle);
                }
            }
            return BadRequest();
        }

        // POST api/vehicles
        //        Request:
        //Body schema: { “id”: string, “vehicleType”: int, “balance”: decimal
        //    }
        //    Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, “balance”: 100 }
        //Response:
        //If body is invalid - Status Code: 400 Bad Request
        //If request is handled successfully
        //Status Code: 201 Created
        //Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
        //Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, "balance": 100 }
        [HttpPost]
        public IActionResult Post([FromBody]Vehicle vehicle)
        {
            if (vehicle == null)
            {
                return BadRequest(vehicle);
            }
            
                parkingService.AddVehicle(vehicle);
                return CreatedAtAction(nameof(Post), vehicle);
            
        }


        // DELETE api/vehicles/id(id - vehicle id of format “AA-0001-AA”)
        //        Response:
        //If id is invalid - Status Code: 400 Bad Request
        //If vehicle not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 204 No Content
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var isMatch = Vehicle.Regex.IsMatch(id);
            if (isMatch)
            {
                var vehicle = parkingService.GetVehicles().SingleOrDefault(v => v.Id == id);
                if (vehicle == null)
                {
                    return NotFound();
                }
                else
                {
                    parkingService.RemoveVehicle(id);
                    return NoContent();
                }

            }
            else
            {
                return BadRequest();
            }
        }
    }
}
