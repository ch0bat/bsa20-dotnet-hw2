﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL;
using System.IO;
using CoolParking.BL.Services;
using System.Reflection;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;

        public ParkingController()
        {
            parkingService = ParkingService.Instance;
        }

        //GET api/parking/balance
        [HttpGet("balance")]
        public ActionResult GetBalance()
        {
            return Ok(parkingService.GetBalance());
        }


        //GET api/parking/capacity
       [HttpGet("capacity")]
       
        public ActionResult GetCapacity()
        {
            return Ok(parkingService.GetCapacity());
        }

        //GET api/parking/freePlaces
        [HttpGet("freePlaces")]

        public ActionResult GetFreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }
       
    }
}