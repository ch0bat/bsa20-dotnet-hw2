﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class TransactionsController : Controller
    {
        IParkingService parkingService;

        public TransactionsController()
        {
            parkingService = ParkingService.Instance;
        }
        // GET: api/<controller>/all
        //        Response:
        //If log file not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: string
        //Body example: “5/10/2020 11:21:20 AM: 3.50 money withdrawn from vehicle with Id = 'GP-5263-GC'.\n5/10/2020 11:21:25 AM: 3.50 money withdrawn from vehicle with Id = 'GP-5263-GC'.”
        [HttpGet("all")]
        public IActionResult GetAll()
        {
            return Ok(parkingService.GetLastParkingTransactions());
        }
        // GET: api/<controller>/last
        //        GET api/transactions/last

        //Response:
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: [{ “vehicleId”: string, “sum”: decimal, "transactionDate": DateTime
        //    }]
        //Body example: [{ “vehicleId”: "DG-3024-UB", “sum”: 3.5, "transactionDate": "2020-05-10T11:36:20.6395402+03:00"}]

        [HttpGet("last")]
        public TransactionInfo GetLast()
        {
            return parkingService.GetLastParkingTransactions().Last<TransactionInfo>();
        }

        // PUT api/transactions/topUpVehicle
        //        Body schema: { “id”: string, “Sum”: decimal
        //    }
        //    Body example: { “id”: “GP-5263-GC”, “Sum”: 100 }
        //Response:
        //If body is invalid - Status Code: 400 Bad Request
        //If vehicle not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
        //Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 245.5 }
        [HttpPut("topUpVehicle")]
        public IActionResult Put([FromBody]TopUpVehicleDto model)
        {
            var isMatch = Vehicle.Regex.IsMatch(model.VehicleId);
            if (isMatch && model.Sum is decimal)
            {
                parkingService.TopUpVehicle(model.VehicleId, model.Sum);
                if (parkingService.GetVehicles().SingleOrDefault(v => v.Id == model.VehicleId) != null)
                {
                    return Ok(parkingService.GetVehicles().SingleOrDefault(v => v.Id == model.VehicleId));
                }
                else
                {
                    return NotFound(parkingService.GetVehicles().SingleOrDefault(v => v.Id == model.VehicleId));
                }
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
