**My CoolParking program**

---

## All tests passed one by one (with singleton)

![Image alt](./Images/1.jpg)

---
## All tests run simultaneously (with singleton)

![Image alt](./Images/3.jpg)

---
## All tests run simultaneously (without singleton)

![Image alt](./Images/4.jpg)

---

## Console Menu

![Image alt](./Images/2.jpg)

---


1. Show parking balance
2. Show sum of transactions of current period
3. Show free places
4. Show all transactions for current period
5. Show history transactions from log
6. Show vehicles list in the parking
7. Add vehicle
8. Remove vehicle
9. Top Up Vehicle
0. Close aplication
Choose number:


**My CoolParking program with Web.API**
---
GET api/parking/balance


Response:
If request is handled successfully
Status Code: 200 OK
Body schema: decimal
Body example: 10.5
---
GET api/parking/capacity


Response:
If request is handled successfully
Status Code: 200 OK
Body schema: int
Body example: 10
---
GET api/parking/freePlaces


Response:
If request is handled successfully
Status Code: 200 OK
Body schema: int
Body example: 9
---
GET api/vehicles


Response:
If request is handled successfully
Status Code: 200 OK
Body schema: [{ “id”: string, “vehicleType”: int, "balance": decimal }]
Body example: [{ “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }]
---
GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)


Response:
If id is invalid - Status Code: 400 Bad Request
If vehicle not found - Status Code: 404 Not Found
If request is handled successfully
Status Code: 200 OK
Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }
---
POST api/vehicles


Request:
Body schema: { “id”: string, “vehicleType”: int, “balance”: decimal }
Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, “balance”: 100 }
Response:
If body is invalid - Status Code: 400 Bad Request
If request is handled successfully
Status Code: 201 Created
Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, "balance": 100 }
---
DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)


Response:
If id is invalid - Status Code: 400 Bad Request
If vehicle not found - Status Code: 404 Not Found
If request is handled successfully
Status Code: 204 No Content
---
GET api/transactions/last


Response:
If request is handled successfully
Status Code: 200 OK
Body schema: [{ “vehicleId”: string, “sum”: decimal, "transactionDate": DateTime }]
Body example: [{ “vehicleId”: "DG-3024-UB", “sum”: 3.5, "transactionDate": "2020-05-10T11:36:20.6395402+03:00"}]
---
## GET api/transactions/all (тільки транзакції з лог файлу)


Response:
If log file not found - Status Code: 404 Not Found
If request is handled successfully
Status Code: 200 OK
Body schema: string
Body example: “5/10/2020 11:21:20 AM: 3.50 money withdrawn from vehicle with Id='GP-5263-GC'.\n5/10/2020 11:21:25 AM: 3.50 money withdrawn from vehicle with Id='GP-5263-GC'.”
---
PUT api/transactions/topUpVehicle


Body schema: { “id”: string, “Sum”: decimal }
Body example: { “id”: “GP-5263-GC”, “Sum”: 100 }
Response:
If body is invalid - Status Code: 400 Bad Request
If vehicle not found - Status Code: 404 Not Found
If request is handled successfully
Status Code: 200 OK
Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 245.5 }

1. Show parking balance
2. Show parkin capacity
3. Show free places
4. Show all transactions
5. Show last transaction
6. Show vehicles list in the parking
7. Add vehicle
8. Remove vehicle
9. Top Up Vehicle
10. Show vehicles by Id
0. Close aplication
Choose number:
